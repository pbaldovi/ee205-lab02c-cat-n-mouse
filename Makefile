###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build Cat `n Mouse program
###
### @author  Paulo Baldovi <pbaldovi@hawaii.edu>
### @date    25 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = catNmouse

all: $(TARGET)

catNmouse: catNmouse.c
	$(CC) $(CFLAGS) -o $(TARGET) catNmouse.c

clean:
	rm -f $(TARGET) *.o

